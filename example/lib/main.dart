import 'package:flutter/material.dart';
import 'package:jumping_bottom_nav_bar_flutter/source/tab_bar.dart';
import 'package:jumping_bottom_nav_bar_flutter/source/tab_icon.dart';
import 'package:jumping_bottom_nav_bar_flutter/source/sheet_bar.dart';

//reference for bottom navigation: https://www.youtube.com/watch?v=qeeiDuRihyE&ab_channel=RetroPortalStudio

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Title',
      home: BottomBarPage(),
    );
  }
}

class BottomBarPage extends StatefulWidget {
  @override
  _BottomBarPageState createState() => _BottomBarPageState();
}

class _BottomBarPageState extends State<BottomBarPage> {
  int selectedIndex = 1;

  final iconList = [
    TabIcon(
        iconData: Icons.home,
        startColor: Colors.black,
        endColor: Colors.yellow),
    TabIcon(
        iconData: Icons.healing,
        startColor: Colors.black,
        endColor: Colors.yellowAccent),
    TabIcon(
        iconData: Icons.person,
        startColor: Colors.black,
        endColor: Colors.yellowAccent),
  ];

  void onChangeTab(int index) {
    selectedIndex = index;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: iconList.length,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "Bottom Navigation and Sheet",
            style: TextStyle(color: Colors.yellow),
          ),
          backgroundColor: Colors.black.withOpacity(0.8),
        ),
        body: TabBarView(
          children: [
            new Container(
              child: Center(
                child: MyRaisedButton(text: 'FIRST TAB')
              ),
            ),
            new Container(
              child: Center(
                  child: MyRaisedButton(text: 'SECOND TAB')
              ),
            ),
            new Container(
              child: Center(
                  child: MyRaisedButton(text: 'THIRD TAB')
              ),
            ),
          ],
        ),
        bottomNavigationBar: Jumping_Bar(
          color: Colors.yellow,
          onChangeTab: onChangeTab,
          duration: Duration(seconds: 1),
          circleGradient: RadialGradient(
            colors: [
              Colors.black,
              Colors.black,
              // Colors.grey.shade100,
              // Colors.red.shade900,
            ],
          ),
          items: iconList,
          selectedIndex: selectedIndex,
        ),
      ),
    );
  }
}

class MyRaisedButton extends StatefulWidget {
  final String text;

  const MyRaisedButton({Key key, @required this.text}): super(key: key);

  @override
  _MyRaisedButtonState createState() => _MyRaisedButtonState();
}

class _MyRaisedButtonState extends State<MyRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.only(left: 60, top: 15, right: 60, bottom: 15),
      color: Colors.black,
      onPressed: () {
        showBottomSheet(
            context: context, builder: (context) => BottomSheetWidget());
      },
      child: Text(
        widget.text,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.yellow),
      ),
    );
  }
}
