import 'package:flutter/material.dart';

class BottomSheetWidget extends StatefulWidget {
  const BottomSheetWidget({Key key}) : super(key: key);

  @override
  _BottomSheetWidgetState createState() => _BottomSheetWidgetState();
}

class _BottomSheetWidgetState extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5, left: 15, right: 15),
      height: 180,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 175,
            decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      blurRadius: 5, color: Colors.black38, spreadRadius: 3)
                ]),
            child: Column(
              children: <Widget>[
                DecoratedTitle(),
                DecoratedTextField(),
                SheetButton()
              ],
            ),
          )
        ],
      ),
    );
  }
}

class DecoratedTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Text(
        'Bottom Sheet Bar',
        style: TextStyle(
            fontSize: 20, color: Colors.yellow, fontWeight: FontWeight.w500),
      ),
    );
  }
}

class DecoratedTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      alignment: Alignment.center,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        decoration:
            InputDecoration.collapsed(hintText: 'Write a comment if you like'),
      ),
    );
  }
}

class SheetButton extends StatefulWidget {
  SheetButton({Key key}) : super(key: key);

  _SheetButtonState createState() => _SheetButtonState();
}

class _SheetButtonState extends State<SheetButton> {
  bool postComment = false;
  bool success = false;

  @override
  Widget build(BuildContext context) {
    return !postComment
        ? MaterialButton(
            color: Colors.yellow,
            onPressed: () async {
              setState(() {
                postComment = true;
              });

              await Future.delayed(Duration(seconds: 1));

              setState(() {
                success = true;
              });

              await Future.delayed(Duration(milliseconds: 500));

              Navigator.pop(context);
            },
            child: Text(
              'Post',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          )
        : !success
            ? Container(
                padding: EdgeInsets.only(left: 25, right: 25),
                child: LinearProgressIndicator(
                  minHeight: 10,
                  backgroundColor: Colors.grey[800],
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.yellow),
                ),
              )
            : Icon(
                Icons.check,
                color: Colors.yellow,
              );
  }
}
